pipeline {
	agent {
		node {
			label 'master'
		}
	}  
	 stages {
		stage('Build') {
			steps {				 
				 sh '''#!/bin/sh +x
				 	echo "Passing #!/bin/sh -e - Running Test.sh"
					./test.sh 08 prl e2e
				'''							
				}
			}		
		stage('Test') {
			steps {
				sh '''#!/bin/bash -x

				echo "prints the command as well"

				set +x
				echo "this just prints the echo's output"

				set -x
				echo "also prints the command itself again"
				'''
			}
			
		}
	 }
}
